package fr.whitericeteam.tictactoe3d;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

public class TTTPlayersActivity extends Activity
{
    TTTDB tttdb ;
    ArrayList<String> listeplayers;
    ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.players);
        ((TextView)findViewById(R.id.textplayers)).setText(
                getResources().getString(R.string.btn_players));
        ((Button)findViewById(R.id.btninsert)).setText(
                getResources().getString(R.string.btn_insert));
        tttdb = new TTTDB(this);
        tttdb.open();
        final ArrayList<TTTPlayer> players = tttdb.getPlayers();
        if(players == null )
            return ;

        else
        {
            listeplayers = new ArrayList<>();
            for(int i = 0 ; i < players.size() ; ++i)
            {
                listeplayers.add(String.format(getResources().getString(R.string.str_player),
                        players.get(i).getName(), players.get(i).getScore()));
            }
            adapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listeplayers);
            ListView list = (ListView)findViewById(R.id.listView2);
            list.setAdapter(adapter);
            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.player_remove),
                            Toast.LENGTH_SHORT).show();
                    tttdb.open();
                    String selectedItem = listeplayers.get(position);
                    String name = listeplayers.get(position).split(",")[0];
                    adapter.remove(selectedItem);
                    adapter.notifyDataSetChanged();
                    tttdb.removePlayerWithName(name);
                    tttdb.close();
                    return false;
                }
            });
            tttdb.close();
        }
    }

    public void addItem(String s)
    {
        if (listeplayers == null)
            listeplayers = new ArrayList<>();
        listeplayers.add(s);
        if (adapter == null)
        {
            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listeplayers);
            ((ListView)findViewById(R.id.listView2)).setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();
    }

    public void insertplayer(View view)
    {
        tttdb.open();
        TTTPlayer newplayer = new TTTPlayer();
        EditText insertname = (EditText) findViewById(R.id.editplayer);
        String name = insertname.getText().toString();
        newplayer.setName(name);
        if(tttdb.getPlayerWithName(name) == null)
        {
            tttdb.insertPlayer(newplayer);
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.player_created),
                    Toast.LENGTH_SHORT).show();
            addItem(name+", score : 0");
        }
        else
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.player_exists),
                    Toast.LENGTH_SHORT).show();
        tttdb.close();
    }
}
