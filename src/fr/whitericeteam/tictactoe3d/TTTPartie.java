package fr.whitericeteam.tictactoe3d;

import java.util.Date;

public class TTTPartie {

    TTTPlayer gagnant;
    TTTPlayer perdant;
    int nbCoup;
    long date;

    public TTTPartie(TTTPlayer p1, TTTPlayer p2, int coup)
    {
        this.gagnant = p1;
        this.perdant = p2;
        this.nbCoup  = coup;
        this.date = new Date().getTime();
    }
    public TTTPartie(TTTPlayer p1, TTTPlayer p2, int coup, long date)
    {
        this.gagnant = p1;
        this.perdant = p2;
        this.nbCoup  = coup;
        this.date = date;
    }

    public TTTPlayer getGagnant()
    {
        return this.gagnant;
    }
    public TTTPlayer getPerdant()
    {
        return this.perdant;
    }

    public int getNbCoup() {
        return nbCoup;
    }

    public long getDate()
    {
        return this.date;
    }
}