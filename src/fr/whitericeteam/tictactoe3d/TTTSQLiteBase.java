package fr.whitericeteam.tictactoe3d;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;


public class TTTSQLiteBase extends SQLiteOpenHelper
{
    private static String DB_PLAYER_TABLE = "player_table";
    private static String DB_PARTIE_TABLE = "partie_table";
    private static String COL_ID_PLAYER = "id_player";
    private static String COL_ID_GAGNANT = "id_gagnant";
    private static String COL_ID_PERDANT = "id_perdant";
    private static String COL_NAME_PLAYER= "name_player";
    private static String COL_SCORE = "score";
    private static String COL_NB_COUP = "coup";
    private static String COL_DATE = "date";

    private static final String CREATE_PLAYER_TABLE = "CREATE TABLE " + DB_PLAYER_TABLE + "(" + COL_ID_PLAYER + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NAME_PLAYER + " TEXT NOT NULL UNIQUE,"+COL_SCORE +" TEXT NOT NULL);";

    private static final String CREATE_PARTIE_TABLE = "CREATE TABLE " + DB_PARTIE_TABLE + "("+ COL_ID_GAGNANT + " INTEGER, "+ COL_ID_PERDANT + " INTEGER, "+ COL_NB_COUP+" INTEGER, " +COL_DATE+" INTEGER, PRIMARY KEY(" + COL_ID_GAGNANT + ", " + COL_ID_PERDANT + ", " + COL_DATE + "));";


    //constructor de DB
    public TTTSQLiteBase(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    //methode appelee si la base n'existe pas ou est vide
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_PLAYER_TABLE);
        db.execSQL(CREATE_PARTIE_TABLE);
        Log.i(null,CREATE_PLAYER_TABLE);
        Log.i(null,CREATE_PARTIE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + DB_PLAYER_TABLE + ";");
        db.execSQL("DROP TABLE " + DB_PARTIE_TABLE + ";");
        onCreate(db);
    }
}