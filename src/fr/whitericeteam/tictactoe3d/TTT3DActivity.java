package fr.whitericeteam.tictactoe3d;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class TTT3DActivity extends Activity
{
    private int player;
    private String[] names;
    private ImageButton[][][] grid;
    private int[][][] checkedGrid;
    private boolean finished;
    int turn;
    private TTTDB tttdb;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ttt3d);
        ((Button)findViewById(R.id.restart)).setText(
                getResources().getString(R.string.restart));
        player = new Random().nextInt(2) + 1;
        names = new String[2];
        Bundle bundle = getIntent().getExtras();
        names[0] = bundle.getString("p1");
        names[1] = bundle.getString("p2");
        tttdb = new TTTDB(this);
        ((TextView)findViewById(R.id.textView)).setText(String.format(
                getResources().getString(R.string.player_turn),
                names[player - 1]));
        finished = false;
        turn = 0;
        grid = new ImageButton[][][] {
                {
                        {
                                ((ImageButton)findViewById(R.id.up0)),
                                ((ImageButton)findViewById(R.id.up1)),
                                ((ImageButton)findViewById(R.id.up2))
                        },
                        {
                                ((ImageButton)findViewById(R.id.up3)),
                                ((ImageButton)findViewById(R.id.up4)),
                                ((ImageButton)findViewById(R.id.up5))
                        },
                        {
                                ((ImageButton)findViewById(R.id.up6)),
                                ((ImageButton)findViewById(R.id.up7)),
                                ((ImageButton)findViewById(R.id.up8))
                        }
                },
                {
                        {
                                ((ImageButton)findViewById(R.id.md0)),
                                ((ImageButton)findViewById(R.id.md1)),
                                ((ImageButton)findViewById(R.id.md2))
                        },
                        {
                                ((ImageButton)findViewById(R.id.md3)),
                                ((ImageButton)findViewById(R.id.md4)),
                                ((ImageButton)findViewById(R.id.md5))
                        },
                        {
                                ((ImageButton)findViewById(R.id.md6)),
                                ((ImageButton)findViewById(R.id.md7)),
                                ((ImageButton)findViewById(R.id.md8))
                        }
                },
                {
                        {
                                ((ImageButton)findViewById(R.id.bw0)),
                                ((ImageButton)findViewById(R.id.bw1)),
                                ((ImageButton)findViewById(R.id.bw2))
                        },
                        {
                                ((ImageButton)findViewById(R.id.bw3)),
                                ((ImageButton)findViewById(R.id.bw4)),
                                ((ImageButton)findViewById(R.id.bw5))
                        },
                        {
                                ((ImageButton)findViewById(R.id.bw6)),
                                ((ImageButton)findViewById(R.id.bw7)),
                                ((ImageButton)findViewById(R.id.bw8))
                        }
                }
        };
        checkedGrid = new int[3][][];
        for (int i = 0; i < 3; i++)
        {
            checkedGrid[i] = new int[3][];
            for (int j = 0; j < 3; j++)
                checkedGrid[i][j] = new int[] {0, 0, 0};
        }
        reset(null);
    }

    private void changePlayer()
    {
        player = player % 2 + 1;
        turn++;
        ((TextView)findViewById(R.id.textView)).setText(
                String.format(getResources().getString(R.string.player_turn),
                        names[player - 1]));
    }

    public void squareClick(View view)
    {
        if (finished)
            return;
        int[] pos = findPosition(grid, (ImageButton) view);
        if (pos == null)
            return;
        if (checkedGrid[pos[0]][pos[1]][pos[2]] == 0)
        {
            if (player == 1) {
                view.setBackgroundResource(R.drawable.cross);
            } else {
                view.setBackgroundResource(R.drawable.circle);
            }
            checkedGrid[pos[0]][pos[1]][pos[2]] = player;
            if (checkGrid(0) || checkGrid(1) || checkGrid(2) ||
                    checkHGrid(0) || checkHGrid(1) || checkHGrid(2)||
                    checkH2Grid(0) || checkH2Grid(1) || checkH2Grid(2) || checkGDiag())
            {
                Toast.makeText(getApplicationContext(),
                        String.format(getResources().getString(R.string.str_win),
                                names[player - 1], (turn / 2 + 1)),
                        Toast.LENGTH_SHORT).show();
                win(player, (turn / 2 + 1));
                finished = true;
            }
            changePlayer();
            if (isFull() && !finished)
            {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.game_over),
                        Toast.LENGTH_SHORT).show();
                finished = true;
            }
        }
    }

    private void win(int p, int t)
    {
        tttdb.open();
        TTTPlayer gagnant = tttdb.getPlayerWithName(names[p - 1]);
        TTTPlayer perdant = tttdb.getPlayerWithName(names[2 - p]);
        tttdb.insertPartie(new TTTPartie(gagnant, perdant, t));
        tttdb.incrScore(gagnant);
        tttdb.close();
    }

    private int[] findPosition(ImageButton[][][] grid, ImageButton btn)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    if (grid[i][j][k] == btn)
                        return new int[] {i, j, k};
        return null;
    }

    private boolean isFull()
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                    if (checkedGrid[i][j][k] == 0)
                        return false;
        return true;
    }

    private boolean checkGrid(int z)
    {
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[z][i][j] != 0)
                while (j < 3 && checkedGrid[z][i][j - 1] == checkedGrid[z][i][j])
                    j++;
            if (j == 3)
                return true;
        }
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[z][j][i] != 0)
                while (j < 3 && checkedGrid[z][j - 1][i] == checkedGrid[z][j][i])
                    j++;
            if (j == 3)
                return true;
        }
        if (checkedGrid[z][1][1] != 0)
        {
            int i = 1;
            while (i < 3 && checkedGrid[z][i - 1][i - 1] == checkedGrid[z][i][i])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[z][3 - i][i - 1] == checkedGrid[z][2 - i][i])
                i++;
            if (i == 3)
                return true;
        }
        return false;
    }

    private boolean checkHGrid(int z)
    {
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[i][z][j] != 0)
                while (j < 3 && checkedGrid[i][z][j - 1] == checkedGrid[i][z][j])
                    j++;
            if (j == 3)
                return true;
        }
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[j][z][i] != 0)
                while (j < 3 && checkedGrid[j - 1][z][i] == checkedGrid[j][z][i])
                    j++;
            if (j == 3)
                return true;
        }
        if (checkedGrid[1][z][1] != 0)
        {
            int i = 1;
            while (i < 3 && checkedGrid[i - 1][z][i - 1] == checkedGrid[i][z][i])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[3 - i][z][i - 1] == checkedGrid[2 - i][z][i])
                i++;
            if (i == 3)
                return true;
        }
        return false;
    }

    private boolean checkH2Grid(int z)
    {
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[i][j][z] != 0)
                while (j < 3 && checkedGrid[i][j - 1][z] == checkedGrid[i][j][z])
                    j++;
            if (j == 3)
                return true;
        }
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[j][i][z] != 0)
                while (j < 3 && checkedGrid[j - 1][i][z] == checkedGrid[j][i][z])
                    j++;
            if (j == 3)
                return true;
        }
        if (checkedGrid[1][1][z] != 0)
        {
            int i = 1;
            while (i < 3 && checkedGrid[i - 1][i - 1][z] == checkedGrid[i][i][z])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[3 - i][i - 1][z] == checkedGrid[2 - i][i][z])
                i++;
            if (i == 3)
                return true;
        }
        return false;
    }

    private boolean checkGDiag()
    {
        if (checkedGrid[1][1][1] != 0)
        {
            int i = 1;
            while (i < 3 && checkedGrid[i - 1][i - 1][i - 1] == checkedGrid[i][i][i])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[i - 1][3 - i][i - 1] == checkedGrid[i][2 - i][i])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[i - 1][i - 1][3 - i] == checkedGrid[i][i][2 - i])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[i - 1][3 - i][3 - i] == checkedGrid[i][2 - i][2 - i])
                i++;
            if (i == 3)
                return true;
        }
        return false;
    }

    public void reset(View view)
    {
        finished = false;
        turn = 0;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                for (int k = 0; k < 3; k++)
                {
                    grid[i][j][k].setBackgroundResource(R.drawable.empty);
                    checkedGrid[i][j][k] = 0;
                }
    }
}
