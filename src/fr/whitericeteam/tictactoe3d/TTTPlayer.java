package fr.whitericeteam.tictactoe3d;

public class TTTPlayer {


    private int idPlayer;
    private String namePlayer;
    private int score ;

    public TTTPlayer(){}

    public TTTPlayer(int id, String nom, int score)
    {
        this.idPlayer = id;
        this.namePlayer = nom;
        this.score = score;
    }

    public String getName()
    {
        return this.namePlayer;
    }

    public int getId()
    {
        return this.idPlayer;
    }

    public void setName(String name)
    {
        this.namePlayer = name;
    }
    public void setId(int id)
    {
        this.idPlayer = id;
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}