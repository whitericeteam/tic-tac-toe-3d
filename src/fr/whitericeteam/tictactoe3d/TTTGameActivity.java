package fr.whitericeteam.tictactoe3d;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;

public class TTTGameActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    TTTDB tttdb;
    String savedsp1, savedsp2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tttdb = new TTTDB(this);
        savedsp1 = "";
        savedsp2 = "";
        ((Button)findViewById(R.id.play2d)).setText(
                String.format(getResources().getString(R.string.play), 2));
        ((Button)findViewById(R.id.play3d)).setText(
                String.format(getResources().getString(R.string.play), 3));
        ((TextView)findViewById(R.id.textplayer1)).setText(
                String.format(getResources().getString(R.string.player_num), 1));
        ((TextView)findViewById(R.id.textplayer2)).setText(
                String.format(getResources().getString(R.string.player_num), 2));
        ((Button)findViewById(R.id.btnhist)).setText(
                getResources().getString(R.string.btn_hist));
        ((Button)findViewById(R.id.btnplayers)).setText(
                getResources().getString(R.string.btn_players));
        refreshSpinners();
    }

    private void refreshSpinners()
    {
        Spinner sp1 = ((Spinner)findViewById(R.id.spinner));
        Spinner sp2 = ((Spinner)findViewById(R.id.spinner2));
        ArrayList<String> l = new ArrayList<>();
        l.add(getResources().getString(R.string.spinner_placeholder));
        tttdb.open();
        ArrayList<TTTPlayer> players = tttdb.getPlayers();
        if (players != null)
            for (TTTPlayer player : players)
                l.add(player.getName());
        ArrayAdapter<String> apt = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, l);
        sp1.setAdapter(apt);
        sp2.setAdapter(apt);
        if (!savedsp1.equals(""))
            sp1.setSelection(apt.getPosition(savedsp1));
        if (!savedsp2.equals(""))
            sp2.setSelection(apt.getPosition(savedsp2));
        tttdb.close();
    }

    public void start2dGame(View view)
    {
        startGame(new Intent(this, TTT2DActivity.class), 1);
    }

    public void start3dGame(View view)
    {
        startGame(new Intent(this, TTT3DActivity.class), 2);
    }

    public void startGame(Intent intent, int code)
    {
        Spinner sp1 = ((Spinner)findViewById(R.id.spinner));
        Spinner sp2 = ((Spinner)findViewById(R.id.spinner2));
        if (sp1.getSelectedItem().equals(getResources().getString(R.string.spinner_placeholder)) ||
                sp2.getSelectedItem().equals(getResources().getString(R.string.spinner_placeholder)))
        {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.game_activity_select_players),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (sp1.getSelectedItem().equals(sp2.getSelectedItem().toString()))
        {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.game_activity_select_different_players),
                    Toast.LENGTH_SHORT).show();
            return;
        }
        intent.putExtra("p1", sp1.getSelectedItem().toString());
        savedsp1 = sp1.getSelectedItem().toString();
        intent.putExtra("p2", sp2.getSelectedItem().toString());
        savedsp2 = sp2.getSelectedItem().toString();
        startActivityForResult(intent, code);
    }

    public void startHist(View view)
    {
        Intent intent = new Intent(this, TTTHistoryActivity.class);
        startActivityForResult(intent, 3);
    }

    public void starPlayers(View view)
    {
        Intent intent = new Intent(this, TTTPlayersActivity.class);
        startActivityForResult(intent, 4);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        refreshSpinners();
    }
}
