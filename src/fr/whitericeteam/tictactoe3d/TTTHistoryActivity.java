package fr.whitericeteam.tictactoe3d;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class TTTHistoryActivity extends Activity
{
    TTTDB tttdb;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        ((TextView)findViewById(R.id.histtitle)).setText(
                getResources().getString(R.string.btn_hist));
        tttdb = new TTTDB(this);
        tttdb.open();
        ArrayList<TTTPartie> parties = tttdb.getParties();
        if(parties !=null)
        {
            String[] listeparties = new String[parties.size()];
            for (int i = 0; i < listeparties.length; ++i) {
                listeparties[i] = String.format(getResources().getString(R.string.str_hist),
                        parties.get(i).getGagnant().getName(),
                        parties.get(i).getPerdant().getName(),
                        parties.get(i).getNbCoup());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listeparties);
            ListView list = (ListView) findViewById(R.id.listView);
            list.setAdapter(adapter);

        }
        tttdb.close();
    }
}
