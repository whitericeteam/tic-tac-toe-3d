package fr.whitericeteam.tictactoe3d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TTTDB {
    private static final int VERSION_BDD = 1;
    private static String DB_NAME ="TTTDB.db";

    private static final String DB_PLAYER_TABLE = "player_table";
    private static final String COL_ID_PLAYER = "id_player";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NAME_PLAYER = "name_player";
    private static final int NUM_COL_NAME = 1;
    private static final String COL_SCORE_PLAYER = "score";
    private static final int NUM_COL_SCORE = 2;

    private static final String DB_PARTIE_TABLE = "partie_table";
    private static final String COL_ID_GAGNANT = "id_gagnant";
    private static final int NUM_COL_ID_GAGNANT = 0;
    private static final String COL_ID_PERDANT = "id_perdant";
    private static final int NUM_COL_ID_PERDANT = 1;
    private static final String COL_NB_COUP = "coup";
    private static final int NUM_COL_COUP = 2;
    private static final String COL_DATE = "date";
    private static final int NUM_COL_DATE = 3;


    private SQLiteDatabase tttdata_base;

    private TTTSQLiteBase tttsqLiteBase;

    public TTTDB(Context context)
    {
        tttsqLiteBase = new TTTSQLiteBase(context, DB_NAME, null, VERSION_BDD);
    }

    public void open(){
        tttdata_base = tttsqLiteBase.getWritableDatabase();
    }

    public void close(){
        tttdata_base.close();
    }

    public long insertPartie(TTTPartie partie){
        ContentValues player_score = new ContentValues();
        player_score.put(COL_ID_GAGNANT, partie.getGagnant().getId());
        player_score.put(COL_ID_PERDANT, partie.getPerdant().getId());
        player_score.put(COL_NB_COUP, partie.getNbCoup());
        player_score.put(COL_DATE, partie.getDate());
        return tttdata_base.insert(DB_PARTIE_TABLE, null, player_score);
    }

    public long insertPlayer(TTTPlayer p)
    {
        ContentValues player = new ContentValues();
        player.put(COL_NAME_PLAYER, p.getName());
        player.put(COL_SCORE_PLAYER, p.getScore());
        return tttdata_base.insert(DB_PLAYER_TABLE, null, player);
    }


    public TTTPlayer getPlayerWithName(String name)
    {
        Cursor c = tttdata_base.query(DB_PLAYER_TABLE, new String[] {COL_ID_PLAYER, COL_NAME_PLAYER,COL_SCORE_PLAYER}, COL_NAME_PLAYER + " LIKE \"" + name +"\"", null, null, null, null);
        return cursorToPlayer(c);
    }
    public TTTPlayer getPlayerWithID(int id)
    {
        Cursor c = tttdata_base.query(DB_PLAYER_TABLE, new String[] {COL_ID_PLAYER, COL_NAME_PLAYER,COL_SCORE_PLAYER}, COL_ID_PLAYER + " LIKE " + id, null, null, null, null);
        return cursorToPlayer(c);
    }

    public int removePlayerWithName(String name)
    {
        tttdata_base.delete(DB_PARTIE_TABLE,COL_ID_GAGNANT +"= ? OR " + COL_ID_PERDANT +"= ?" ,
                new String[]{String.valueOf(getPlayerWithName(name).getId()),
                        String.valueOf(getPlayerWithName(name).getId())});
        return tttdata_base.delete(DB_PLAYER_TABLE, COL_NAME_PLAYER + "= ?", new String[] { name });
    }

    private TTTPlayer cursorToPlayer(Cursor c)
    {
        if (c.getCount() == 0)
            return null;
        c.moveToFirst();
        TTTPlayer player = new TTTPlayer();
        player.setId(c.getInt(NUM_COL_ID));
        player.setName(c.getString(NUM_COL_NAME));
        player.setScore(c.getInt(NUM_COL_SCORE));
        c.close();

        return player;
    }

    public ArrayList<TTTPartie> getParties()
    {
        ArrayList<TTTPartie> parties = new ArrayList<>();
        Cursor c = tttdata_base.query(DB_PARTIE_TABLE, new String[] {COL_ID_GAGNANT, COL_ID_PERDANT,COL_NB_COUP,COL_DATE},null,null,null,null,null);
        if (c.getCount() == 0)
            return null;
        if (c.moveToFirst()) {
            do {
                parties.add(new TTTPartie(getPlayerWithID(c.getInt(0)),
                        getPlayerWithID(c.getInt(1)),
                        c.getInt(2), c.getInt(3)));
            } while (c.moveToNext());
        }
        c.close();
        return parties;
    }

    public ArrayList<TTTPlayer> getPlayers()
    {
        ArrayList<TTTPlayer> players = new ArrayList<>();
        Cursor c = tttdata_base.query(DB_PLAYER_TABLE, new String[] {COL_ID_PLAYER,COL_NAME_PLAYER,COL_SCORE_PLAYER},null,null,null,null,null);
        if (c.getCount() == 0)
            return null;
        if (c.moveToFirst()){
            do {
                players.add(new TTTPlayer(c.getInt(0),c.getString(1),c.getInt(2)));
            } while (c.moveToNext());
        }
        c.close();
        return players;
    }

    public long incrScore(TTTPlayer p)
    {
        ContentValues values = new ContentValues();
        p.setScore(p.getScore()+1);
        values.put(COL_SCORE_PLAYER,p.getScore());
        return tttdata_base.update(DB_PLAYER_TABLE, values,COL_ID_PLAYER + " = " +p.getId(), null);

    }

}