package fr.whitericeteam.tictactoe3d;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class TTT2DActivity extends Activity
{
    private int player;
    private String[] names;
    private ImageButton[][] grid;
    private int[][] checkedGrid;
    private boolean finished;
    private int turn;
    private TTTDB tttdb;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ttt2d);
        ((Button)findViewById(R.id.restart)).setText(
                getResources().getString(R.string.restart));
        player = new Random().nextInt(2) + 1;
        names = new String[2];
        Bundle bundle = getIntent().getExtras();
        names[0] = bundle.getString("p1");
        names[1] = bundle.getString("p2");
        tttdb = new TTTDB(this);
        ((TextView)findViewById(R.id.textView)).setText(
                String.format(getResources().getString(R.string.player_turn),
                        names[player - 1]));
        finished = false;
        turn = 0;
        grid = new ImageButton[][] {
                {
                        ((ImageButton)findViewById(R.id.bt0)),
                        ((ImageButton)findViewById(R.id.bt1)),
                        ((ImageButton)findViewById(R.id.bt2))
                },
                {
                        ((ImageButton)findViewById(R.id.bt3)),
                        ((ImageButton)findViewById(R.id.bt4)),
                        ((ImageButton)findViewById(R.id.bt5))
                },
                {
                        ((ImageButton)findViewById(R.id.bt6)),
                        ((ImageButton)findViewById(R.id.bt7)),
                        ((ImageButton)findViewById(R.id.bt8))
                }
        };
        checkedGrid = new int[3][];
        for (int i = 0; i < 3; i++)
        {
            checkedGrid[i] = new int[] {0, 0, 0};
        }
        reset(null);
    }

    private void changePlayer()
    {
        player = player % 2 + 1;
        turn++;
        ((TextView)findViewById(R.id.textView)).setText(
                String.format(getResources().getString(R.string.player_turn),
                        names[player - 1]));
    }

    public void squareClick(View view)
    {
        if (finished)
            return;
        int[] pos = findPosition(grid, (ImageButton) view);
        if (pos == null)
            return;
        if (checkedGrid[pos[0]][pos[1]] == 0)
        {
            if (player == 1) {
                view.setBackgroundResource(R.drawable.cross);
            } else {
                view.setBackgroundResource(R.drawable.circle);
            }
            checkedGrid[pos[0]][pos[1]] = player;
            if (checkGrid())
            {
                Toast.makeText(getApplicationContext(),
                        String.format(getResources().getString(R.string.str_win),
                                names[player - 1], (turn / 2 + 1)),
                                Toast.LENGTH_SHORT).show();
                win(player, (turn / 2 + 1));
                finished = true;
            }
            changePlayer();
            if (isFull() && !finished)
            {
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.game_over),
                        Toast.LENGTH_SHORT).show();
                finished = true;
            }
        }
    }

    private void win(int p, int t)
    {
        tttdb.open();
        TTTPlayer gagnant = tttdb.getPlayerWithName(names[p - 1]);
        TTTPlayer perdant = tttdb.getPlayerWithName(names[2 - p]);
        tttdb.insertPartie(new TTTPartie(gagnant, perdant, t));
        tttdb.incrScore(gagnant);
        tttdb.close();
    }

    private int[] findPosition(ImageButton[][] grid, ImageButton btn)
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (grid[i][j] == btn)
                    return new int[] {i, j};
        return null;
    }

    private boolean isFull()
    {
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                if (checkedGrid[i][j] == 0)
                    return false;
        return true;
    }

    private boolean checkGrid()
    {
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[i][j] != 0)
                while (j < 3 && checkedGrid[i][j - 1] == checkedGrid[i][j])
                    j++;
            if (j == 3)
                return true;
        }
        for (int i = 0; i < 3; i++)
        {
            int j = 1;
            if (checkedGrid[j][i] != 0)
                while (j < 3 && checkedGrid[j - 1][i] == checkedGrid[j][i])
                    j++;
            if (j == 3)
                return true;
        }
        if (checkedGrid[1][1] != 0)
        {
            int i = 1;
            while (i < 3 && checkedGrid[i - 1][i - 1] == checkedGrid[i][i])
                i++;
            if (i == 3)
                return true;
            i = 1;
            while (i < 3 && checkedGrid[3 - i][i - 1] == checkedGrid[2 - i][i])
                i++;
            if (i == 3)
                return true;
        }
        return false;
    }

    public void reset(View view)
    {
        finished = false;
        turn = 0;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
            {
                grid[i][j].setBackgroundResource(R.drawable.empty);
                checkedGrid[i][j] = 0;
            }
    }
}
