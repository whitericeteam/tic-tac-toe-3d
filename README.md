# Tic-Tac-Toe 3D #

*by White Rice Team*

## The Team ##

* Pierre Chat
* Nam Ly

## The Project ##

Tic-Tac-Toe 3D is an Android school project, for [IUT d'Orléans](http://www.univ-orleans.fr/iut-orleans/).
Experience an unique system of three dimensional tic-tic-toe !

## Features ##

* Two players game modes
* Classic two dimensional tic-tac-toe
* Unique system of three dimensional tic-tac-toe with a three-layered view
* Players management
* Games history